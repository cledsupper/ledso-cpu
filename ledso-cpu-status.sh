#!/bin/bash
# ledso-cpu-status.sh - a simple BASH script for checking your CPU current freq.
# by Ledso [Cledson F. Cavalcanti (cledsonitgames@gmail.com)]

ledso_head() {
	echo "";
	echo "ledso-cpu-status.sh";
	echo "";
}

# POLICY0, POLICY1, .... are relative to CPU0, CPU1, .... They are the CPU cores you CPU has.
readonly step0=0; # for symmetry purpose.
readonly stepN=1; # the last core identifier

# CONVERT AND FORMAT VALUE TO FLOAT IN C LOCALE MODE
readonly LC_NUMERIC=C;
readonly cpufreq_folder=/sys/devices/system/cpu/cpufreq/policy;

readonly minfreq_file=scaling_min_freq;
readonly curfreq_file=scaling_cur_freq;
readonly maxfreq_file=scaling_max_freq;

# ARRAYS OF SCALING__FREQ FILES FOR EVERY STEP
readonly files_min=($cpufreq_folder{$step0,$stepN}/$minfreq_file);
readonly files_cur=($cpufreq_folder{$step0,$stepN}/$curfreq_file);
readonly files_max=($cpufreq_folder{$step0,$stepN}/$maxfreq_file);

check_err() {
	if [ $? -ne 0 ]; then
		echo "";
		echo "ON STEP $step: please check if file above exists and you can read!";
		exit 1;
	fi
}

ledso_head;

step=0;
until [ $step -gt $stepN ]; do
	test -e ${files_min[$step]};
	check_err;
	test -e ${files_cur[$step]};
	check_err;
	test -e ${files_max[$step]};
	check_err;

	((step++));
done

while [ $? -eq 0 ]; do
	clear;

	ledso_head;
	echo "Press CTRL+C to quit";

	step=0;
	until [ $step -gt $stepN ]; do
		echo "";
		echo "`cat ${files_min[$step]}` <= CPU $step <= `cat ${files_max[$step]}`";
		echo "CPU $step ≃ `cat ${files_cur[$step]}`";
		((step++));
	done

	echo "";
	echo "by Ledso!"
	sleep 0.5;
done
