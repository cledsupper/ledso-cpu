#!/bin/bash
# ledso-cpu-underclock.sh - a simple BASH script for underclocking your Linux device.
# by Ledso [Cledson F. Cavalcanti (cledsonitgames@gmail.com)]
# DEPENDS OF: GNU bc

######################  DISCLAIMER NOTE  ######################
# Please, DO NOT change anything here except 'FREQ_GHZ' and
# 'stepn'!
#
# BE AWARE:
# I HAVE NO RESPONSABILITIES FOR YOUR CPU BURNING OR ANYTHING
# ELSE THAT WOULD HAPPEN IF YOU DO NOT USE THIS CAREFULLY.
# DO USE OF THIS SINGLE PEARL AT YOUR OWN RISK.
#
####################  DISCLAIMER NOTE END  ####################

ledso_head() {
	echo "";
	echo "ledso-cpu-underclock.sh";
	echo "";
}

# Set I_AM_TESTING to 1 if you're going to suffer in the obscurity and chaos of this code.
readonly I_AM_TESTING=0;

# POLICY0, POLICY1, .... are relative to CPU0, CPU1, .... They are the CPU cores you CPU has.
step=0;
readonly step0=0; # for symmetry purpose.
readonly stepN=1; # the last core identifier

# MAX FREQUENCY IN GIGAHERTZ
readonly FREQ_GHZ=1.1;

# CONVERT AND FORMAT VALUE TO INTEGER IN C LOCALE MODE.
readonly LC_NUMERIC=C;
readonly maxfreq_freq=`printf "%.0lf" "\`bc <<< \"$FREQ_GHZ * 1000000\"\`"`;
readonly maxfreq_folder=/sys/devices/system/cpu/cpufreq/policy;
readonly maxfreq_file=scaling_max_freq;

# ARRAY OF SCALING_MAX_FREQ FILES FOR EVERY STEP
readonly files=($maxfreq_folder{$step0,$stepN}/$maxfreq_file);

check_freq() {
	read maxfreq_cur_max < ${files[$step]};
	if [ $maxfreq_freq -gt $maxfreq_cur_max ]; then
		echo "FATAL: SELECTED FREQ ($maxfreq_freq) IS GREATER THAN DEFAULT MAX FREQ ($maxfreq_cur_max)!";
		exit 2;
	fi
}

check_err() {
	if [ $? -ne 0 ]; then
		echo "";
		echo "ON STEP $step: please check if file above exists and you're root!";
		exit 1;
	fi
	if [ $I_AM_TESTING -eq 1 ]; then
		echo "";
	fi
}

ledso_head;
echo -e "Underclocking by" policy{$step0,$stepN}, "\b\b (rel.:" CPU{$step0,$stepN}, "\b\b)";
echo "Max frequencies will be set to $FREQ_GHZ GHz";
echo "NOTE: this is a temporary setting. After rebooting, max freqs get back to their default values.";
echo "You can see them calling ledso-cpu-status.sh before setting.";
echo "";
echo "Type 'yes' to confirm, anything else to cancel:";
read ures;

if [ "$ures" == "yes" ]; then
	echo "";
	echo "Underclocking selected CPU's...";
	until [ $step -gt $stepN ]; do
		check_freq;

		if [ $I_AM_TESTING -eq 1 ]; then
			echo "echo \"$maxfreq_freq\" > ${files[$step]}";
		else
			echo "$maxfreq_freq" > ${files[$step]};
		fi
		check_err;
		echo "CPU$step: <= $FREQ_GHZ GHz";
		echo "";
		((step++));
	done
fi

echo "";
echo "by Ledso!";
